#!/usr/bin/env nodejs

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const http = require('https');
const dateTime = require('node-datetime');
const async = require('async');

 const EVENTHUB_CONNECTION_STRING="Endpoint=sb://qseventhub.servicebus.windows.net/;SharedAccessKeyName=DefaultAccessKey;SharedAccessKey=4YXj6+yYWDosLHWNObkCgqf7GINEZRrSNESsg/xYcIo=";
 const EVENTHUB_NAME="default";
 const Types = require('./avro_types.js');
 const { EventHubClient } = require('@azure/event-hubs');
 const eventHubClient = EventHubClient.createFromConnectionString(EVENTHUB_CONNECTION_STRING, EVENTHUB_NAME);

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

var _debug = false;

var moment = require('moment');

const UPDATE_TIMEOUT_S = 600; //after this interval (if no kiosk health received) the status turns to N/A

var mysql = require('mysql');
var mysql_pool_s  = mysql.createPool({
  connectionLimit : 2,
  host            : '52.157.176.37',
  user            : 'user',
  password        : 'user',
  database        : 'reporting',
  supportBigNumbers: true,
  bigNumberStrings: true
});
var mysql_pool_m  = mysql.createPool({
  connectionLimit : 2,
  host            : 'localhost',
  user            : 'reportingapp',
  password        : '',
  database        : 'reporting',
  supportBigNumbers: true,
  bigNumberStrings: true
});

class Database {
    constructor( pool ) {
        this.pool = pool
    }

    getConnection() {
        return new Promise( ( resolve, reject ) => {
            this.pool.getConnection( ( err, conn ) => {
                if ( err ) return reject( err );
                resolve( conn );
            });
        });
    }

    beginTransaction( conn ) {
        return new Promise( ( resolve, reject ) => {
            conn.beginTransaction( ( err ) => {
                if (err) reject( err );
                resolve();
            });
        });
    }

    commitTransaction( conn ) {
        return new Promise( ( resolve, reject ) => {
            conn.commit( ( err ) => {
                if (err) {
                    conn.rollback();
                    reject( err );
                }
                resolve();
            });
        });
    }

    queryTransaction( conn, sql, args ) {
        return new Promise( ( resolve, reject ) => {
            conn.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }

    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.pool.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }
}

var dbS = new Database(mysql_pool_s);
var dbM = new Database(mysql_pool_m);

function debug(str) {
	if (_debug) log(str);
}

function log(str) {
	var dt = dateTime.create();
	var formatted = dt.format('Y-m-d H:M:S');
	console.log(formatted,str);
}

function get_between(str,a,b) {
	var re = new RegExp(a+"(.*?)"+b,'g');
	var x = str.match(re);

	if (!x) return "";

	if (x.length!=1) return false;
	x = x[0];

	x = x.slice(0,-b.length);
	x = x.substring(a.length);

	return x;
}

function truncate(str) {
	if (str[str.length-1]==",")
		return str.substring(0,str.length-1);

	return str;
}

 function eventHubSend(type,ptId,ptTS,payload) {
     var event = {
         "type":"LOGEvent",
         "subtype":type,
         "logTS":moment(ptTS).valueOf(),
         "logId":ptId,
         "payload": payload
     }
     eventHubClient.send({body: event});
 }

var arr = [];
var init = false;

function healthNA(r) { //health not available
    if (r.PEDStatus) {
        r.PEDStatus = null;
        r.PEDStatusDirty = true;
    }

    if (r.printerStatus) {
        r.printerStatus = null;
        r.printerStatusDirty = true;
    }
}

function ping() {
    let ct = moment.utc();
    arr.forEach((r,lid)=>{
        let x = moment.duration(ct.diff(r.timestamp)).asSeconds();
        if (isNaN(x)) {
            console.log("Something's wrong?");
            return;
        }
        if (x>UPDATE_TIMEOUT_S) {
            healthNA(r);
        }
    });
}

function updateMem(lid,ped,printer) {
	if (!init) return;
	let r = arr[lid];
    if (!r) return; //location not interesting

    r.timestamp = moment.utc();

    if (r.PEDStatus != ped) {
        r.PEDStatusDirty = true;
        r.PEDStatus = ped;
    }

    if (r.printerStatus != printer) {
        r.printerStatusDirty = true;
        r.printerStatus = printer;
    }

}

var dbRunning = false;
async function updateDB() {
    if (dbRunning) {
        console.log("DB still updating?!");
        return;
    }
    dbRunning = true;
    log("Updating DB...");
    //TODO: for each dirty record update the DB and reset dirty
    //

    let conn = await dbM.getConnection();
    await dbM.beginTransaction(conn);

    var _dirty = false;
    function _pushQuerySet(q,str) {
        if (_dirty) q+=(","+str);
        else q+=str;
        return q;
    }

    var p = []; //promise array;

    arr.forEach((r,lid)=>{
        var q = "";
        var a = [];
        _dirty = false;
        if (r.PEDStatusDirty) {
            //console.log(lid,"PED Changed status",r.PEDStatus);
            r.PEDStatusDirty = false;
            q = _pushQuerySet(q,"PEDStatus=?,PEDStatusTimestamp=now()");
            _dirty = true;
            a.push(r.PEDStatus);
        }
        if (r.printerStatusDirty) {
            //console.log(lid,"Printer Changed status",r.PrinterStatus);
            r.printerStatusDirty = false;
            q = _pushQuerySet(q,"PrinterStatus=?,PrinterStatusTimestamp=now()");
            _dirty = true;
            a.push(r.printerStatus);
        }

        if (_dirty) {
            q = "UPDATE reporting.locationRollup SET "+q+" WHERE QSLID=?;";
            //console.log(q,a);
            a.push(lid);
            p.push(dbM.queryTransaction(conn,q,a));
        }
    });

    await Promise.all(p);
    await dbM.commitTransaction(conn);
    conn.release();

    log("Done");
    dbRunning = false;

}

function initLocation(l) {
	arr[l.QSLID] = {
        PEDStatus: l.PEDStatus,
        printerStatus: l.PrinterStatus,
        timestamp: moment.utc(), //used to mark locations as N/A when health not received for extended time
        PEDStatusDirty: false,
        printerStatusDirty: false
	}
}

const listLocations = async() => { return dbS.query("select QSLID,PEDStatus,PrinterStatus from reporting.locationRollup",[]); };

const main = async() => {
	log("Retrieving list of locations to monitor...");
	let lList = await listLocations();
	lList.map(initLocation);
    log("Initialized");
	init = true;
    mysql_pool_s.end();

    setInterval(ping.bind(this),60*1000);
    setInterval(updateDB.bind(this),5*60*1000);
}

function process_event(e) {
    let msg = JSON.parse(e.message);
    if (!msg || !msg.pedHealthInfo || !msg.printerHealthInfo) {
        log("Error parsing message");
        console.log(e);
    }
    let lid = msg.locationId;
    let PEDStatus = msg.pedHealthInfo.cardReaderReady;
    let printerStatus = msg.printerHealthInfo.printerStatus;

    updateMem(lid,PEDStatus,printerStatus);
}


app.post('/*', function(req, res) {
	var payload;
    var sIP = "";

    try {
        payload = JSON.parse(req.body.payload);
     } catch(e) {
        console.log(sIP+" ERROR PARSING JSON");
        console.log(req.body);
        console.log("DONE");
        throw e;
     }
	debug(sIP+": Got "+payload.events.length+" events");
	let c = 0;
	if (payload.events) {
		payload.events.forEach((e)=>{process_event(e);c++});
		res.end(JSON.stringify({ status: 'SUCCESS' }));
		log(sIP+" Processed "+c+" events.");
	} else {
		log("Error getting events");
		res.end(JSON.stringify({ status: 'ERROR' }));
	}
})

main();

var server = app.listen(8082, function() {
    var host = server.address().address
    var port = server.address().port
});


