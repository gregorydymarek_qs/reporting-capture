#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOG=$DIR/log.txt

START_TIME=$SECONDS
mysql --user=reportingapp -h localhost --database=reporting < $DIR/sql/script.sql >> $LOG 2>&1
ELAPSED_TIME=$(($SECONDS - $START_TIME))
CDATE="$( date +%Y%m%d\ %H:%M:%S )"
echo "$CDATE     SQL Duration: $ELAPSED_TIME s." >> $LOG


START_TIME=$SECONDS
$DIR/sync.js >> $LOG 2>&1
ELAPSED_TIME=$(($SECONDS - $START_TIME))
CDATE="$( date +%Y%m%d\ %H:%M:%S )"
echo "$CDATE     Sync duration: $ELAPSED_TIME s." >> $LOG

