#!/usr/bin/env nodejs

var SERVERURL="https://api.qikserve.net";
var APIKEY="l8pj9h8cc5mgijpn2fwhlihknlnrz1n";

const EVENTHUB_CONNECTION_STRING="Endpoint=sb://qseventhub.servicebus.windows.net/;SharedAccessKeyName=DefaultAccessKey;SharedAccessKey=4YXj6+yYWDosLHWNObkCgqf7GINEZRrSNESsg/xYcIo=";
const EVENTHUB_NAME="default";
const Types = require('./avro_types.js');
const { EventHubClient } = require('@azure/event-hubs');
const eventHubClient = EventHubClient.createFromConnectionString(EVENTHUB_CONNECTION_STRING, EVENTHUB_NAME);


var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var http = require('https');
var dateTime = require('node-datetime');
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

var moment = require('moment');

var mysql = require('mysql');
var mysql_pool  = mysql.createPool({
  connectionLimit : 2,
  host            : 'localhost',
  user            : 'reportingapp',
  password        : '',
  database        : 'reporting',
  supportBigNumbers: true,
  bigNumberStrings: true
});

class Database {
     constructor( pool ) {
         this.pool = pool
     }

     getConnection() {
         return new Promise( ( resolve, reject ) => {
             this.pool.getConnection( ( err, conn ) => {
                 if ( err ) return reject( err );
                 resolve( conn );
             });
         });
     }

     beginTransaction( conn ) {
         return new Promise( ( resolve, reject ) => {
             conn.beginTransaction( ( err ) => {
                 if (err) reject( err );
                 resolve();
             });
         });
     }

     commitTransaction( conn ) {
         return new Promise( ( resolve, reject ) => {
             conn.commit( ( err ) => {
                 if (err) {
                     conn.rollback();
                     reject( err );
                 }
                 resolve();
             });
         });
     }

     queryTransaction( conn, sql, args ) {
         return new Promise( ( resolve, reject ) => {
             conn.query( sql, args, ( err, rows ) => {
                 if ( err )
                     return reject( err );
                 resolve( rows );
             } );
         } );
     }

     query( sql, args ) {
         return new Promise( ( resolve, reject ) => {
             this.pool.query( sql, args, ( err, rows ) => {
                 if ( err )
                     return reject( err );
                 resolve( rows );
             } );
         } );
     }
 }

var db = new Database(mysql_pool);


function log(str) {
	var dt = dateTime.create();
	var formatted = dt.format('Y-m-d H:M:S');
	console.log(formatted,str);
}

function get_between1(str,a,b) {
	var re = new RegExp(a+"(.*?)"+b,'g');
	var x = re.exec(str);

	if (!x) return "";

	x = x[1];

	return x;
}

function get_between(str,a,b) {
	var re = new RegExp(a+"(.*?)"+b,'g');
	var x = str.match(re);

	if (!x) return "";

	if (x.length!=1) return false;
	x = x[0];

	x = x.slice(0,-b.length);
	x = x.substring(a.length);

	return x;
}

function truncate(str) {
	if (str[str.length-1]==",")
		return str.substring(0,str.length-1);

	return str;
}

function formatDateMysql(date) {
  return moment(date).format('YYYY-MM-DD HH:mm:ss');
}

function createCallback(obj,fun) {
    return function() {
    	var args = Array.from(arguments);
    	args.push(obj);
    	fun.apply(this,args);
    }
};

function eventHubSend(type,ptId,ptTS,payload) {
    var event = {
        "type":"LOGEvent",
        "subtype":type,
        "logTS":moment(ptTS).valueOf(),
        "logId":ptId,
        "payload": payload
    }
    eventHubClient.send({body: event});
}

function process_msg_order(e) {
	var rid = truncate(get_between(e.message,"rid="," "));
	var nonce = truncate(get_between(e.message,"orderNonce="," "));
	var lid = truncate(get_between(e.message,"lid="," "));
	var cargo = get_between(e.message,"cargo=","} ") + "}";
	var paymentType = get_between(e.message,"paymentType="," ");
	cargo = JSON.parse(cargo);
    var itemsCount = cargo.categories[0].items.length;
	var papertrailID = e.id;
	var timestamp  = formatDateMysql(e.received_at);
	var timefk = moment(e.received_at).format('YYYYMMDDHH');

	if (isNaN(lid)) {
		lid = 0;
	}
	//console.log("Order: ",nonce, " total: ",cargo.total, " rid: ",rid);
	var obj = {
		QSNonce: nonce,
		QSRestaurantID: rid,
		QSLID: lid,
		TotalPrice: cargo.total/100,
        ItemsCount: itemsCount,
		PapertrailID: papertrailID,
		Timestamp: timestamp,
		TimeFK: timefk
	};

    eventHubSend("OrderStart",e.id,e.received_at,obj);

	mysql_pool.query('INSERT IGNORE INTO reporting.order SET ?', obj, function(err,result) {
		if (err) throw err;
	});

}

function process_msg_pos(e) {
	var nonce = truncate(get_between(e.message,"nonce="," "));
	var order = truncate(get_between(e.message,"order="," "));
	var operator = truncate(get_between(e.message,"operator="," "));
	var timestamp  = formatDateMysql(e.received_at);

	var obj = {
		QSOrderID: order,
		TimestampPOSStart: timestamp,
		QSOperatorID: operator
	};

	if (!isNaN(operator)) {
		obj.QSOperatorID = operator;
	}

    var obj1 = {
		QSOrderID: order,
		QSOperatorID: operator,
        QSNonce: nonce,
        Timestamp: timestamp
    }

    eventHubSend("OrderPOSStart",e.id,e.received_at,obj1);

	mysql_pool.query('UPDATE reporting.order SET ? WHERE QSNonce = ? ', [obj, nonce], function(err,result) {
		if (err) throw err;
		//console.log(result.insertId);
	});

}

function process_msg_pos_ok(e) {
	var nonce = truncate(get_between(e.message,"nonce="," "));
	var checkId = truncate(get_between(e.message,"checkId="," "));
	var timestamp  = formatDateMysql(e.received_at);

	var obj = {
		TimestampPOSEnd: timestamp
	};

	if (isNaN(checkId)) {
        log("Unable to parse CheckID??");
        console.log(e);
		checkId = null;
	} else {
        obj.CheckID = checkId;
    }

    var obj1 = {
        QSNonce: nonce,
        Timestamp: timestamp
    }

    eventHubSend("OrderPOSEnd",e.id,e.received_at,obj1);

	mysql_pool.query('UPDATE reporting.order SET ? WHERE QSNonce = ? ', [obj, nonce], function(err,result) {
		if (err) {
            throw err;
        }
		//console.log(result.insertId);
	});
}

function process_msg_order_ok(e) {
	var nonce = truncate(get_between(e.message,"nonce="," "));
	var operator = truncate(get_between(e.message,"operator="," "));
	var order = truncate(get_between(e.message,"order="," "));
	var timestamp  = formatDateMysql(e.received_at);

	var obj = {
		TimestampComplete: timestamp
	};

	if (!isNaN(operator)) {
		obj.QSOperatorID = operator;
	}

	if (!isNaN(order)) {
		obj.QSOrderID = order;
	}

    var obj1 = {
        QSNonce: nonce,
        Timestamp: timestamp
    }

    eventHubSend("OrderFinished",e.id,e.received_at,obj1);


	//mysql_pool.query('UPDATE reporting.order SET ?, POSSendDuration = UNIX_TIMESTAMP(TimestampPOSEnd)-UNIX_TIMESTAMP(TimestampPOSStart)  WHERE QSNonce = ? ', [obj, nonce], function(err,result) {
	mysql_pool.query('UPDATE reporting.order SET ? WHERE QSNonce = ? ', [obj, nonce], function(err,result) {
		if (err) throw err;
		//console.log(result.insertId);
	});
}

function process_journey_started(e) {
	var lid = truncate(get_between(e.message,"lid="," "));
	var nonce = truncate(get_between(e.message,"nonce="," "));
	var timestamp  = formatDateMysql(e.received_at);
	var timefk = moment(e.received_at).format('YYYYMMDDHH');
	var papertrailID = e.id;

	if (nonce.length==0) {
		nonce = null;
	}

	//console.log(lid,timestamp);
	lid = parseInt(lid);

	if (isNaN(lid)) {
		console.log("Unable to parse journeyStarted.",papertrailID);
		return;
	}

	var obj = {
		QSLID: lid,
		Timestamp: timestamp,
		TimeFK: timefk,
		QSNonce: nonce
	};

    eventHubSend("JourneyStarted",e.id,e.received_at,obj);

	mysql_pool.query('INSERT INTO reporting.journey SET ?', obj, function(err,result) {
		if (err) throw err;
		//console.log(result.insertId);
	});
}

function process_postotals(e) {
	var papertrailID = e.id;
	var lid = truncate(get_between(e.message,"lid="," "));
	var rid = truncate(get_between(e.message,"rid="," "));
	var nonce = truncate(get_between(e.message,"orderNonce="," "));
	var logTrace = truncate(get_between(e.message,"logTraceId="," "));
	var timestamp  = formatDateMysql(e.received_at);
	var timefk = moment(e.received_at).format('YYYYMMDDHH');

	lid = parseInt(lid);

	if (isNaN(lid)) {
		console.log("Unable to parse lid for postotals.",papertrailID,e.message);
		return;
	}

	rid = parseInt(rid);

	if (isNaN(rid)) {
		console.log("Unable to parse rid for postotals. Is this an injected test order?",papertrailID,e.message);
		return;
	}

	var obj = {
		QSLID: lid,
		QSRestaurantID: rid,
		Timestamp: timestamp,
		QSNonce: nonce,
		QSLogTraceID: logTrace,
		TimeFK: timefk
	};

    eventHubSend("POSTotals",e.id,e.received_at,obj);

	mysql_pool.query('INSERT IGNORE INTO reporting.calcTotals SET ?', obj, function(err,result) {
		if (err) throw err;
		//console.log(result.insertId);
	});
}

function process_postotals_start(e) {
	var nonce = truncate(get_between(e.message,"nonce="," "));
	var timestamp  = formatDateMysql(e.received_at);
	var logTrace = truncate(get_between(e.message,"logTraceId="," "));
    var lid = truncate(get_between(e.message,"location="," "));

	var obj = {
		TimestampStart: timestamp
	};

    var obj1 = {
        "lid": lid,
        QSLogTraceID: logTrace,
		Timestamp: timestamp
    }

    eventHubSend("POSTotalsStart",e.id,e.received_at,obj1);

	mysql_pool.query('UPDATE reporting.calcTotals SET ? WHERE QSLogTraceID = ? ', [obj, logTrace], function(err,result) {
		if (err) throw err;
		//console.log(result.insertId);
	});
}

function process_postotals_end(e) {
	var nonce = truncate(get_between(e.message,"nonce="," "));
	var qsduration = truncate(get_between(e.message,"posCommsTime="," "));
	var logTrace = truncate(get_between(e.message,"logTraceId="," "));
    var lid = truncate(get_between(e.message,"location="," "));
	var p_qsduration = parseFloat(qsduration);
	if (isNaN(p_qsduration)) {
		console.log("Error parsing posTotals. Nonce: ",nonce,qsduration);
		return;
	}
	var timestamp  = formatDateMysql(e.received_at);

	var obj = {
		TimestampEnd: timestamp,
		QSDuration: p_qsduration
	};

    var obj1 = {
        "lid": lid,
        QSLogTraceID: logTrace,
		Timestamp: timestamp,
		QSDuration: p_qsduration
    }

    eventHubSend("POSTotalsEnd",e.id,e.received_at,obj1);

	mysql_pool.query('UPDATE reporting.calcTotals SET ? WHERE QSLogTraceID = ? ', [obj, logTrace], function(err,result) {
		if (err) throw err;
		//console.log(result.insertId);
	});
}

var lid_ping = [];
function process_kiosk_poscheck(e) {
	var lid = truncate(get_between(e.message,"/posCheck/","'"));
	var timestamp  = formatDateMysql(e.received_at);
    lid_ping[lid] = timestamp;


    var obj = {
        "lid": lid
    }

    eventHubSend("KioskPing",e.id,e.received_at,obj);
}

async function update_lid_ping_db() {
    let conn = await db.getConnection();
    await db.beginTransaction(conn);

    var p = []; //promise array;

    lid_ping.forEach((v,k)=>{
        p.push(db.queryTransaction(conn,"INSERT INTO reporting.ping (QSLID,Timestamp) values (?,?)",[k,v]));
    });

    await Promise.all(p);
    await db.commitTransaction(conn);
    conn.release();
    lid_ping = [];
}

function process_poscheck(e) {
	var rid = truncate(get_between(e.message,"restaurant="," "));
	var result = 0;
	var timestamp  = formatDateMysql(e.received_at);
	var timefk = moment(e.received_at).format('YYYYMMDDHH');
	var papertrailID = e.id;

	if (e.message.includes(" Successful.")) result=1;
	else result=0;

	rid = parseInt(rid);

	if (isNaN(rid)) {
		console.log("Unable to parse posCheck.",papertrailID,timefk,e.message);
		return;
	}

	var obj = {
		QSRestaurantID: rid,
		Timestamp: timestamp,
		TimeFK: timefk,
		Result: result
	};

    eventHubSend("POSCheck",e.id,e.received_at,obj);

	mysql_pool.query('INSERT INTO reporting.posCheck SET ?', obj, function(err,result) {
		if (err) throw err;
		//console.log(result.insertId);
	});
}


function process_event(e) {
	//e.id
	//get_nonce(e.message);
	//e.received_at
	//console.log("ev");
	//console.log(e.message);

	//if (e.message.includes("MenuController")) process_menu_get(e);
	if (e.message.includes("PosCheckService")) {
        process_poscheck(e);
        //special case - we want to know when we loose PosChecks
        return 1;
    }
	else if (e.message.includes("GET '/posCheck/")) process_kiosk_poscheck(e);
	else if (e.message.includes("GET '/journeyStarted'")) process_journey_started(e);
	else if (e.message.includes("POST '/orders_async'")) process_msg_order(e);
	else if (e.message.includes("POST '/order'")) process_msg_order(e);
	else if (e.message.includes("POST '/addToOpenCheck'")) process_msg_order(e);
	else if (e.message.includes("POST '/posTotals'")) process_postotals(e);
	else if (e.message.includes("Calculating POS totals... OK!")) process_postotals_end(e);
	else if (e.message.includes("Calculating POS totals...")) process_postotals_start(e);
	else if (e.message.includes("Sending order to POS... OK!")) process_msg_pos_ok(e);
	else if (e.message.includes("Sending order to POS...")) process_msg_pos(e);
	else if (e.message.includes("Order process finished.")) process_msg_order_ok(e);
	else {
		//unhandled message
	}

    return 0;

}

app.post('/*/delay/:c', function(req, res) {
	var payload = JSON.parse(req.body.payload);
	if (payload.events) {
		log("Got event (delayed): "+payload.events.length);
		setTimeout(function() {
			payload.events.forEach(process_event);
			},req.params.c*1000);
        //log("Done.");
		res.end(JSON.stringify({ status: 'SUCCESS' }));
	} else {
		log("Error getting print events");
		res.end(JSON.stringify({ status: 'ERROR' }));
	}
})

app.post('/*/print_stats/:c', function(req, res) {
	var payload = JSON.parse(req.body.payload);
	var lid = req.params.c;
	console.log("Print Stats for: "+lid);
	console.log(payload);
	res.end(JSON.stringify({ status: 'SUCCESS' }));
})

app.post('/*', function(req, res) {
    var sIP = "";
	var payload;
    try {
        payload = JSON.parse(req.body.payload);
    } catch(e) {
        console.log(sIP+" ERROR PARSING JSON");
        console.log(req.body);
        console.log("DONE");
        throw e;
    }
	if (payload.events) {
		log(sIP+": Got events: "+payload.events.length);
        var r = 0;
		payload.events.forEach((e)=>{
            r+=process_event(e);
        });
        log("PosCheckService count: "+r);
		res.end(JSON.stringify({ status: 'SUCCESS' }));
        //log("Done.");
	} else {
		log("Error getting events");
		res.end(JSON.stringify({ status: 'ERROR' }));
	}
})

var server = app.listen(8080, function() {
    var host = server.address().address
    var port = server.address().port
});

setInterval(update_lid_ping_db,60000);

log("Server started.");

