# populate time table with hours/days/months/years

DELIMITER $$
DROP PROCEDURE IF EXISTS populate_time$$

CREATE PROCEDURE populate_time()
BEGIN
	DECLARE c INT DEFAULT 0;
    DECLARE a INT DEFAULT 360; #calculate 360 front
    DECLARE y INT;
    DECLARE m INT;
    DECLARE d INT;
    DECLARE h INT;
    DECLARE id INT;
    DECLARE t_now DATETIME;
	DECLARE t DATETIME;

	SET t_now = DATE_FORMAT(NOW(), "%Y-%m-%d %H:00:00");

    WHILE (c < a) DO
    
	SET t = DATE_ADD(t_now, INTERVAL c HOUR);
    
	SET y = YEAR(t);
    SET m = MONTH(t);
    SET d = DAY(t);
    SET h = HOUR(t);
    
	SET id = y* 1000000 +
		m* 10000 +
        d* 100 +
        h;
    
INSERT IGNORE INTO time (TimeID,Year,Month,Day,Hour,Timestamp) 
	values (
		id,
		y,
		m,
		d,
		h,
        t
	);
    


        SET c = c + 1;
    END WHILE;
END$$
DELIMITER ;

