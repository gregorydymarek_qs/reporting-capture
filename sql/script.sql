USE reporting;
CALL populate_time();

UPDATE reporting.order set QSNonce=NULL WHERE QSNonce is not null and Timestamp+INTERVAL 30 DAY < NOW();

DELETE from reporting.posCheck where Timestamp+INTERVAL 3 DAY < NOW();
DELETE from reporting.ping where Timestamp+INTERVAL 30 DAY < NOW();
DELETE from reporting.log where Timestamp+INTERVAL 30 DAY < NOW();
DELETE from reporting.journey where Timestamp+INTERVAL 95 DAY < NOW();
DELETE from reporting.calcTotals where Timestamp+INTERVAL 30 DAY < NOW();

-- order, payment, time ...?

