#!/usr/bin/env nodejs

var SERVERURL="https://api.qikserve.net";
var APIKEY="l8pj9h8cc5mgijpn2fwhlihknlnrz1n";

const dateTime = require('node-datetime');
const moment = require('moment');
const async = require('async');
var bodyParser = require('body-parser');
var express = require('express');
var app = express();
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
const mysql = require('mysql');

var mysqlPool  = mysql.createPool({
  connectionLimit : 2,
  host            : 'localhost',
  user            : 'reportingapp',
  password        : '',
  database        : 'reporting',
  supportBigNumbers: true,
  bigNumberStrings: true
});

class Database {
     constructor( pool ) {
         this.pool = pool
     }

     getConnection() {
         return new Promise( ( resolve, reject ) => {
             this.pool.getConnection( ( err, conn ) => {
                 if ( err ) return reject( err );
                 resolve( conn );
             });
         });
     }

     beginTransaction( conn ) {
         return new Promise( ( resolve, reject ) => {
             conn.beginTransaction( ( err ) => {
                 if (err) reject( err );
                 resolve();
             });
         });
     }

     commitTransaction( conn ) {
         return new Promise( ( resolve, reject ) => {
             conn.commit( ( err ) => {
                 if (err) {
                     conn.rollback();
                     reject( err );
                 }
                 resolve();
             });
         });
     }

     queryTransaction( conn, sql, args ) {
         return new Promise( ( resolve, reject ) => {
             conn.query( sql, args, ( err, rows ) => {
                 if ( err )
                     return reject( err );
                 resolve( rows );
             } );
         } );
     }

     query( sql, args ) {
         return new Promise( ( resolve, reject ) => {
             this.pool.query( sql, args, ( err, rows ) => {
                 if ( err )
                     return reject( err );
                 resolve( rows );
             } );
         } );
     }
 }

const getContent = function(url) {
  // return new pending promise
  return new Promise((resolve, reject) => {
    // select http or https module, depending on reqested url
    const lib = url.startsWith('https') ? require('https') : require('http');
    const request = lib.get(url, (response) => {
      // handle http errors
      if (response.statusCode < 200 || response.statusCode > 299) {
         reject(new Error('Failed to load page, status code: ' + response.statusCode));
       }
      // temporary data holder
      const body = [];
      // on every content chunk, push it to the data array
      response.on('data', (chunk) => body.push(chunk));
      // we are done, resolve promise with those joined chunks
      response.on('end', () => resolve(JSON.parse(body.join(''))));
    });
    // handle connection errors of the request
    request.on('error', (err) => reject(err))
    })
};

var db = new Database(mysqlPool);

function log(str) {
	var dt = dateTime.create();
	var formatted = dt.format('Y-m-d H:M:S');
	console.log(formatted,str);
}

function formatDateMysql(date) {
    return moment(date).format('YYYY-MM-DD HH:mm:ss');
}

var conn;

async function init() {
    conn = await db.getConnection();
    return db.beginTransaction(conn);
}

async function cleanUp() {
    await db.commitTransaction(conn);
    conn.release();
	mysqlPool.end();
	log('Done');
}

log('Running...');

const listOperators = async() => { return db.query("select OperatorID,QSOperatorID from reporting.operator"); };
const getRestaurants = async(oid) => { return getContent(SERVERURL+"/operator/"+oid+"/restaurants?apiKey="+APIKEY); };
const getLocations = async(rid) => { return getContent(SERVERURL+"/tables?rid="+rid); };
const dbRestaurantSync = async(r) => { return db.queryTransaction(conn,"INSERT INTO reporting.restaurant (QSRestaurantID,Name,Currency,QSOperatorID,GoLiveDate,Latitude,Longitude) VALUES (?,?,?,?,?,?,?) on DUPLICATE KEY UPDATE Name=?,Currency=?,QSOperatorID=?,GoLiveDate=?, Latitude=?, Longitude=?",[r.id,r.name,r.currency,r._oid,r.goLive,r.geographicCoordinate.latitude,r.geographicCoordinate.longitude,r.name,r.currency,r._oid,r.goLive,r.geographicCoordinate.latitude,r.geographicCoordinate.longitude]); };
const dbLocationSync = async(s) => { return db.queryTransaction(conn,"INSERT INTO reporting.location (QSLID,Name,QSRestaurantID,IncludeInReporting,isTable) VALUES (?,?,?,?,?) on DUPLICATE KEY UPDATE QSLID=?,Name=?,QSRestaurantID=?,IncludeInReporting=?,IsTable=?",[s.id,s.name,s._rid,s.includeInReporting,s._isTable,s.id,s.name,s._rid,s.includeInReporting,s._isTable]); };
const dbFinalize1 = async() => { return db.queryTransaction(conn,"update reporting.restaurant set _HasReporting=1 where QSRestaurantID in (select QSRestaurantID from reporting.location where IncludeInReporting=1)"); };
const dbFinalize2 = async() => { return db.queryTransaction(conn,"update reporting.restaurant set _HasReporting=0 where QSRestaurantID not in (select QSRestaurantID from reporting.location where IncludeInReporting=1)"); };

const doRestaurants = async(o) => {
	let r = await getRestaurants(o.QSOperatorID);
    r.cargo.restaurants.forEach((v)=>{
        v._oid=o.QSOperatorID;
        if (v.goLive) v.goLive=formatDateMysql(v.goLive);
    });
	let p = r.cargo.restaurants.map(dbRestaurantSync);
    p = p.concat(r.cargo.restaurants.map(doLocations));
	await Promise.all(p);
}

const doLocations = async(r) => {
	let locs = await getLocations(r.id);
    locs.cargo.locations.forEach((v)=>{
        v._rid=r.id;
        v._isTable = v.number?1:0;
    });
	locs = locs.cargo.locations.map(dbLocationSync);
	await Promise.all(locs);
}

const main = async() => {
    await init();

    let oList = await listOperators();
	let p = oList.map(doRestaurants);
    await Promise.all(p);

	await dbFinalize1();
	await dbFinalize2();

	await cleanUp();
}

main();

